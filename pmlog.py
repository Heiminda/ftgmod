#    Friendly Telegram (telegram userbot)
#    Copyright (C) 2018-2019 The Authors

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import asyncio
import logging

from telethon import types
from telethon.errors import MessageIdInvalidError

from .. import loader, utils

logger = logging.getLogger(__name__)


@loader.tds
class PMLogMod(loader.Module):
    """Logs unwanted PMs to a channel"""
    strings = {"name": "PM Logger",
               "start": "<b>Conversation added to the List</b>",
               "not_pm": "<b>You can't log a group</b>",
               "stopped": "<b>Conversation removed from the List</b>",
               "log_group_cfg_doc": "Group or channel ID where to send the logged PMs",
               "whitelist_doc": "Whether the list is a for excluded or included chats",
               "whitelist_on": "<b>Changed the list to a Whitelist.</b>",
               "whitelist_off": "<b>Changed the list to a Blacklist.</b>",
               "pmlogbot_on": "<b>Conversations with bots will be logged.</b>",
               "pmlogbot_off": "<b>Conversations with bots won't be logged.</b>"}

    def __init__(self):
        self.config = loader.ModuleConfig("LOG_GROUP", None, lambda m: self.strings("log_group_cfg_doc", m))
        self._ratelimit = []

    async def addpmloglistcmd(self, message):
        """Adds conversation to the list"""
        if not message.is_private or message.to_id.user_id == (await message.client.get_me(True)).user_id:
            await utils.answer(message, self.strings("not_pm", message))
            return
        self._db.set(__name__, "users", list(set(self._db.get(__name__, "users", []) + [message.to_id.user_id])))
        msgs = await utils.answer(message, self.strings("start", message))
        await asyncio.sleep(1)
        await message.client.delete_messages(message.to_id, msgs)

    async def rempmloglistcmd(self, message):
        """Removes conversation from the list"""
        if not message.is_private or message.to_id.user_id == (await message.client.get_me(True)).user_id:
            await utils.answer(message, self.strings("not_pm", message))
            return
        self._db.set(__name__, "users",
                     list(set(self._db.get(__name__, "users", [])).difference([message.to_id.user_id])))
        await utils.answer(message, self.strings("stopped", message))

    async def pmloglistcmd(self, message):
        """toggles between whitelist and blacklist"""
        pmlog_whitelist = self._db.get(__name__, "whitelist")
        if pmlog_whitelist is None or not pmlog_whitelist:
            self._db.set(__name__, "whitelist", True)
            await utils.answer(message, self.strings("whitelist_on", message))
        elif pmlog_whitelist is True:
            self._db.set(__name__, "whitelist", False)
            await utils.answer(message, self.strings("whitelist_off", message))

    async def pmlogbotscmd(self, message):
        """toggles if whether chats with bots should be included or not"""
        pmlogbot = self._db.get(__name__, "pmlogbot")
        if pmlogbot is None or not pmlogbot:
            self._db.set(__name__, "pmlogbot", True)
            await utils.answer(message, self.strings("pmlogbot_on", message))
        elif pmlogbot is True:
            self._db.set(__name__, "pmlogbot", False)
            await utils.answer(message, self.strings("pmlogbot_off", message))

    async def watcher(self, message):
        if not message.is_private or not isinstance(message, types.Message):
            return
        pmlog_whitelist = self._db.get(__name__, "whitelist")
        pmlogbot = self._db.get(__name__, "pmlogbot")
        chat = await message.get_chat()
        if chat.bot and not pmlogbot:
            return
        if utils.get_chat_id(message) in self._db.get(__name__, "users", []):
            chatidindb = True
        else:
            chatidindb = False
        if pmlog_whitelist and chatidindb or not pmlog_whitelist and not chatidindb:
            return
        if self.config["LOG_GROUP"]:
            name = chat.username
            linkid = str(chat.id)
            link = "Chat: <a href='tg://user?id=" + linkid + "'>" + name + "</a>\nID: " + linkid
            try:
                await message.forward_to(self.config["LOG_GROUP"])
                await message.client.send_message(self.config["LOG_GROUP"], link)
                return
            except MessageIdInvalidError:
                return

    async def client_ready(self, client, db):
        self._db = db
