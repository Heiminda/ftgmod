# FTGmod

My friendly-telegram userbot modules repo. I didn't like few things in official repo so I changed few things around and made this repository

## Warning!

This is a repo for modules made especially for [Friendly-Telegram](https://github.com/friendly-telegram), that are optimized and working only on [Friendly-Telegram](https://github.com/friendly-telegram) userbot.

## About

I collected unofficial modules from [FTG UserBot Modules](https://t.me/FTGModules) channel of HitaloSama,translated to English and added some modules from [DEKFTGMODULES](https://t.me/dekftgmodules) of D4n1l3k300 also done same for [FTG Modules by Fl1yd](https://ftgmodulesbyfl1yd) of Fl1yd and some other sources which I found to be working.
I will test and add more modules in future :P

## What is removed?

• AFK and NoPM modules - This module is in DND module so nothing is missing.
• Official weather module replaced by new weather module (based on wttr.in) that has no API dependency.
• Misc module - it is a useless module anyway kek.

## Installation

1. Copy this link:
```
 'https://gitlab.com/Heiminda/ftgmod/-/raw/master'
```
2. Add this link to the FTG module loader settings
3. Restart userbot.
4. Done, now use ".dlmod" in some chat and see the modules.

## Word of advice
Install RaphielGang, PPEX and Uniborg dependencies. I think atleast RaphielGang dependencies is necessary. 
Don't you know how to install them?
Check out my channel [FTGmod](https://t.me/ftgmod)
